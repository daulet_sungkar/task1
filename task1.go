package main

import (
	"fmt"
	"net/http"
	"text/template"
	"time"

	"github.com/patrickmn/go-cache"
)

var (
	m       = make(map[string]*cache.Cache)
	tmpl, _ = template.ParseFiles("index.html")
	banTime = time.Now()
)

func timeToString(t time.Time) string {
	return time.Time.String(t)
}
func GetIp(r *http.Request) string {
	ip := r.Header.Get("X-FORWARDED-FOR")
	if ip != "" {
		return ip
	}
	return r.RemoteAddr
}
func handler(w http.ResponseWriter, r *http.Request) {
	ip := GetIp(r)
	_, found := m[ip]
	if !found {
		m[ip] = cache.New(1*time.Minute, 1*time.Second)
	}
	if banTime.Before(time.Now()) {
		tmpl.Execute(w, ip)
		m[ip].Add(timeToString(time.Now()), ip, 1*time.Minute)
		if m[ip].ItemCount() >= 100{
			banTime = time.Now().Add(2 * time.Minute)
		}
		fmt.Println(m[ip].ItemCount())
		fmt.Println(time.Now(), banTime)
	} else {
		tmp_429, _ := template.ParseFiles("429.html")
		tmp_429.Execute(w, ip)
	}
}
func main() {
	http.HandleFunc("/", handler)
	http.ListenAndServe(":8080", nil)
}
